#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#define SIZE (1)
#define TOTAL (30000000)


char buff[SIZE];


int main(int argc, char* argv[]) {


     unsigned int writecount = TOTAL / SIZE;
     printf("Processing \n");

     unsigned int i=0;


    int fd = open("file.txt", O_CREAT | O_TRUNC | O_RDWR);
    clock_t start = clock();
    for (i; i<writecount;i++){

    write (fd,&buff[0],sizeof(buff));

    }

    clock_t stop = clock();
    double duration = (stop - start)/(double)CLOCKS_PER_SEC;
    printf("TimeELapsed: %f \n", duration);
    printf("WriteCount: %d \n",i);
    printf("buffSize: %d \n" ,sizeof(buff));




    close(fd);

    printf("End");



   return 0;
}
